import React, { FunctionComponent } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { Home } from './Home';
import { Hero } from './Hero';

export const App: FunctionComponent = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/hero">
          <Hero />
        </Route>
      </Switch>
    </Router>
  );
}