import styled from 'styled-components';

export const Wrapper = styled.section`
  text-align: center;
  padding: 30px;
`;

export const InputWrapper = styled.div`
  max-width: 500px;
  margin: auto;
`;

export const HeroesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;