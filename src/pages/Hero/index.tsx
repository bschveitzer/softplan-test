import React, { FunctionComponent, useState, useEffect } from 'react';
import { Wrapper } from './style';
import { selectedHero } from '../../api/graphql/apollo.cache';
import { HeroDescription } from '../../components/HeroDescription';
import { useLazyQuery } from "@apollo/react-hooks";
import { SEARCH_HEROES_BY_ID } from '../../api/graphql/queries/Heroes';

export const Hero: FunctionComponent = () => {
  const [hero, setHero] = useState(selectedHero());

  const [getHeroWithStorage, { data }] = useLazyQuery(SEARCH_HEROES_BY_ID);

  useEffect(() => {
    if(!hero.name && localStorage.getItem('heroId')) {
      const heroID = localStorage.getItem('heroId');
      getHeroWithStorage({variables: {search_term: parseInt(heroID || '')}});
    }
  },[hero, getHeroWithStorage]);

  useEffect(() => {
    if(data) {
      setHero(data.characters[0]);
    } 
  },[data, setHero]);

  return (
    <Wrapper>
      <HeroDescription hero={hero} />
    </Wrapper>
  );
}
