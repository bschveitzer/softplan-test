import React, { FunctionComponent, useState, useEffect } from 'react';
import { Wrapper, InputWrapper, HeroesWrapper, LoadingWrapper } from './style';
import { Typography, Spin } from 'antd';
import { SearchInput } from '../../components/SearchInput';
import { HeroCard } from '../../components/HeroCard';
import { useLazyQuery, useQuery } from "@apollo/react-hooks";
import { SEARCH_HEROES_BY_NAME, SEARCH_ALL_HEROES } from '../../api/graphql/queries/Heroes';
import { IHero } from '../../interfaces/Hero';
import { selectedHero, initialHeroes } from '../../api/graphql/apollo.cache';
import { useHistory } from "react-router-dom";

const { Title, Paragraph } = Typography;

export const Home: FunctionComponent = () => {
  const [heroes, setHeroes] = useState<IHero[]>([]);
  const history = useHistory();
  // Utiliza o método LazyQuery para ser ativado apenas após a finalização da digitação,
  // conta com a ajuda do debounce dentro do SearchInput. A query busca pelo termo inserido no input;
  const [handleSearch, { loading, data }] = useLazyQuery(SEARCH_HEROES_BY_NAME);

  const response = useQuery(SEARCH_ALL_HEROES);

  useEffect(() => {
    if(response.data) {
      initialHeroes(response.data.characters);
      setHeroes(response.data.characters);
    }
  },[response, setHeroes])
  
  useEffect(() => {
    if(data) {
      setHeroes(data.characters);
    } else if (!data) {
      setHeroes(initialHeroes());
    }
  },[data, setHeroes])

  const handleClick = (hero: IHero) => {
    selectedHero(hero);
    localStorage.setItem('heroId', hero.id);
    history.push('/hero')
  }

  return (
    <Wrapper>
      <Title>Busque por seus heróis favoritos</Title>
      <Paragraph>Essa é uma aplicação para busca de heróis na base de dados da Marvel, usado como teste de desenvolvedor de software.</Paragraph>
      <InputWrapper>
        <SearchInput request={(searchTerm: string) => handleSearch({variables: {search_term: searchTerm}})}
          placeholder='Busque por seu herói da Marvel'/>
      </InputWrapper>
      <HeroesWrapper>
        { !loading && heroes.length > 0 && heroes.map((hero, key) => (<HeroCard key={key} hero={hero} onClick={handleClick}/>)
        )}
      </HeroesWrapper>
      { loading && 
        <LoadingWrapper>
          <Spin size="large"/>
          <Title>Buscando heróis...</Title>
        </LoadingWrapper>}
    </Wrapper>
  );
}
