import React, { FunctionComponent, useState, useEffect } from 'react';
import { Wrapper, HeroImage, ContentWrapper, InfoWrapper, ButtonWrapper, InputWrapper } from './style'; 
import { Card, Descriptions, Button, Input } from 'antd';
import { IHeroDescription, ISerie } from '../../interfaces/Hero';

const { Item } = Descriptions;
const { TextArea } = Input;

export const HeroDescription: FunctionComponent<IHeroDescription> = ({ hero }) => {
  const [name, setName] = useState(hero.name);
  const [description, setDescription] = useState(hero.description);
  const [editableName, setEditableName] = useState(hero.name);
  const [editableDescription, setEditableDescription] = useState(hero.description);
  const [editMode, setEditMode] = useState(false);

  const handleConfirmClick = () => {
    setName(editableName);
    setDescription(editableDescription);
    setEditMode(false);
  }

  const handleCancelClick = () => {
    setEditableName(name);
    setEditableDescription(description);
    setEditMode(false);
  }

  useEffect(() => {
    setName(hero.name);
    setDescription(hero.description);
    setEditableName(hero.name);
    setEditableDescription(hero.description);
  },[hero]);

  const renderActions = () => {
    if(editMode) {
      return (
        <ButtonWrapper>
          <Button onClick={handleCancelClick}>Cancelar</Button>
          <Button type="primary" onClick={handleConfirmClick}>Confirmar</Button>
        </ButtonWrapper>
      )
    } else {
      return (
        <Button type="primary" danger onClick={() => setEditMode(true)}>Editar</Button>
      )
    }
  }

  const renderTitle = () => {
    if(editMode) {
      return (
        <InputWrapper>
          <Input value={editableName} onChange={e => setEditableName(e.target.value)}/>
        </InputWrapper>
      )
    } else {
      return name;
    }
  }
  return (
    <Wrapper>
      { hero.name && 
        <Card bodyStyle={{padding: 0}}>
          <ContentWrapper>
            <HeroImage alt="hero-img" src={hero.thumbnail}/>
            <InfoWrapper>  
              <Descriptions title={renderTitle()} extra={renderActions()}>
                <Item label="Descrição">{
                  (!editMode && (description || 'Não possui descrição')) ||
                  (editMode && <TextArea
                    value={editableDescription}
                    autoSize={{ minRows: 3, maxRows: 5 }}
                    onChange={e => setEditableDescription(e.target.value)}
                  />)
                }</Item>
                <Item label="Séries">
                  { hero.series.map((serie: ISerie) => (serie.name + '; '))}
                </Item>
              </Descriptions> 
            </InfoWrapper>
          </ContentWrapper>
        </Card>
      }
    </Wrapper>
  )
}