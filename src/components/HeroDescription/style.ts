import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 15px;
  text-align: initial;

  @media (max-width: 767px) {
    margin: 0;
  }
`;

export const HeroImage = styled.img`
  height: 320px;

  @media (max-width: 767px) {
    height: 280px;
  }
`
export const ContentWrapper = styled.div`
  display: flex;
  padding: 24px;

  @media (max-width: 1025px) {
    display: block;
    padding: 10px;
  }
`
export const InfoWrapper = styled.div`
  margin: 10px;
`

export const ButtonWrapper = styled.div`
  button {
    margin-left: 5px;
    @media (max-width: 767px) {
      font-size: 12px;
      padding: 2px 10px;
    }
  }
  diplay: flex;
`
export const InputWrapper = styled.div`
  width: 200px;
  @media (max-width: 767px) {
    width: 120px;
  }
`