import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 15px;

  @media (max-width: 767px) {
    margin: 0;
  }
`;

export const HeroImage = styled.img`
  height: 320px;
`