import React, { FunctionComponent } from 'react';
import { Wrapper, HeroImage } from './style'; 
import { Card } from 'antd';
import { IHeroCardProps } from '../../interfaces/Hero';

const { Meta } = Card;

export const HeroCard: FunctionComponent<IHeroCardProps> = ({ hero, onClick }) => {
  const handleClick = () => {
    onClick(hero);
  }

  return (
    <Wrapper>
      <Card
        hoverable
        style={{ width: 320 }}
        cover={<HeroImage alt="hero-img" src={hero.thumbnail} />}
        onClick={handleClick}
      >
        <Meta 
          title={hero.name} 
          description='Clique para mais informações'
        />
      </Card>
    </Wrapper>
  )
}
