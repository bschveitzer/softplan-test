import React, { useState, useCallback, FunctionComponent } from 'react';
import debounce from 'lodash.debounce';
import { Input } from 'antd';
import { Wrapper } from './style'; 

export const SearchInput: FunctionComponent<SearchInputProps> = ({placeholder, request}) => {
  const [inputValue, setInputValue] = useState('');

  const debouncedSave = useCallback(
    debounce((newValue: string) => request(newValue), 1000),
    []
  );

  const updateValue = (newValue: string) => {
    setInputValue(newValue);
    debouncedSave(newValue);
  }
  
  return (
    <Wrapper>
      <Input
        name='search-input'
        value={inputValue} 
        onChange={input => updateValue(input.target.value)} 
        placeholder={placeholder}/>
    </Wrapper>
  )
}

interface SearchInputProps {
  placeholder?: string,
  request: (arg: string) => void
}