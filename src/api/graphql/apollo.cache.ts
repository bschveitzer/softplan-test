import { ReactiveVar, makeVar } from "@apollo/client";
import { IHero } from '../../interfaces/Hero'

export const selectedHero: ReactiveVar<IHero> = makeVar<IHero>(
  {
    name: '',
    id: '',
    thumbnail: '',
    description: '',
    series: []
  }
);

export const initialHeroes: ReactiveVar<IHero[]> = makeVar<IHero[]>([]);