import { gql } from '@apollo/client';

// Graphql query que busca os heróis dado um certo termo.
// Params: search_term.
export const SEARCH_HEROES_BY_NAME = gql`
    query($search_term: String!) {
      characters(where: { name: $search_term}) {
        id,
        thumbnail,
        name
        description,
        series {
          name,
          role,
          type
        }
      }
    }
`;
// Graphql query que busca por todos os herois, fixado em um máximo de 5 e ordenado pela data de atualização (mais recente).
export const SEARCH_ALL_HEROES = gql`
    query {
      characters(offset: 5, orderBy: modified_desc) {
        id,
        thumbnail,
        name,
        description,
        series {
          name,
          role,
          type
        }
      }
    }
`;

// Graphql query que busca os heróis dado um certo id.
// Params: id.
export const SEARCH_HEROES_BY_ID = gql`
    query($search_term: Int!) {
      characters(where: { id: $search_term}) {
        id,
        thumbnail,
        name
        description,
        series {
          name,
          role,
          type
        }
      }
    }
`;