export interface IHero {
  id: string,
  thumbnail: string,
  name: string,
  description: string,
  series: ISerie[]
}

export interface IHeroDescription {
  hero: IHero,
}

export interface ISerie {
  name: string,
}

export interface IHeroCardProps {
  hero: IHero,
  onClick: (arg: IHero) => void
}