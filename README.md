# Softplan test

Essa aplicação serve para buscar por heróis na base de dados da Marvel, utiliza um servidor de GraphQL para fazer as requisições, o MarvelQL (https://github.com/Novvum/MarvelQL),para fazer as requisições no client side foi utilizado a ferramenta Apollo (https://www.apollographql.com/). O mesmo Apollo foi utilizado para gerenciar o estado da aplicação, não sendo necessário o uso do Redux.  
Na parte de layout foi utilizado a biblioteca AntD (https://ant.design/) na parte de componentes de interface juntamente com o Styled Components (https://styled-components.com/).  
A base da aplicação foi criada com o create-react-app (https://create-react-app.dev/) já seguindo o template de typescript.  
Para a parte de testes foi utilizado o react-testing-library(https://testing-library.com/) junto com o Jest(https://jestjs.io/).  
Foi configurado um lint para melhor escrita de código.  

ATENÇÃO! A APLICAÇÃO BUSCA 5 HEROIS COMO SUGESTÃO NA PAGINA INICIAL E BUSCA DE HEROIS, SENDO ASSIM, DEVE-SE RODAR O SERVIDOR DO MARVELQL ANTES! UTILIZAR O .ENV PARA SETAR A URL BASE DO SERVIDOR.

## Considerações

O teste em si englobou muitas ferramentas que já conhecia, principalmente por estar usando React, porém foi interessante conhecer o local state do Apollo e voltar a usar o GraphQL em si. A parte de testes é complicada porque depende muito do jeito que a equipe lida, juntamente com a minha "média" experiencia com teste, podem ter ficados bem básicos.  

Agradeço a oportunidade de participar e fico a disposição para qualquer dúvida.

## Scripts

### `yarn`

Para instalar todas as dependências, necessário!

### `yarn lint`
Verificar os erros de escrita de código.

### `yarn start`

Para rodar a aplicação em si, vai inicializar um servidor de desenvolvimento no [http://localhost:3000](http://localhost:3000).

### `yarn test`

Para rodar todos os casos de teste da aplicação.